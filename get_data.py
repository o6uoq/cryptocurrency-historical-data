import pandas as pd
import numpy as np
import datetime
import logging
import time
import os
import asyncio
import aiohttp
from os.path import dirname, join
import random

logger = logging.getLogger()
handler = logging.StreamHandler()
formatter = logging.Formatter('[%(asctime)s] p%(process)s {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s','%m-%d %H:%M:%S')
handler.setFormatter(formatter)
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

PROXIES = os.environ.get("PROXIES", None)
DELAY = 1

# function to read csv files
def _read_file(filename):
    return pd.read_csv(join(dirname(__file__), filename), parse_dates=True, infer_datetime_format=True)

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    chunks = []
    for i in range(0, len(lst), n):
        chunks.append(lst[i:i + n])
    return chunks

# function call api
async def candles(symbol='btcusd', interval='1m', limit=1000, start=None, end=None, sort=-1):
    async with aiohttp.ClientSession() as session:
        url = 'https://api.bitfinex.com/v2/candles/trade:{}:t{}/hist?limit={}&start={:.0f}&end={:.0f}&sort=-1'.format(interval, symbol.upper(), limit, start, end, sort)
        logger.debug("getting url: {}".format(url))
        results = None
        try:
            if PROXIES:
                proxy = random.choice(PROXIES)
                logger.debug("got random proxy: {}".format(proxy))
                resp = await session.get(url, proxy=proxy)
            else:
                resp = await session.get(url)
            await asyncio.sleep(DELAY)
            results = await resp.json()
            return results
        except Exception as e:
            logger.error("Failed to get resp: {}".format(e))
 
        if (not results) or "error" in results:
            # recursive retry
            logger.error("Got rate limited. Trying symbol: {} start: {} end: {} again".format(symbol, start, end))
            return await candles(symbol=symbol, interval=interval, limit=limit, start=start, end=end)
        else:
            return results

# Create a function to fetch the data
async def fetch_data(start=1364767200000, stop=1545346740000, symbol='btcusd', interval='1m', tick_limit=1000, step=60000000):
    # Create api instance

    datas = []
    tasks = []

    # build a tasks list
    if (stop - start) > step:
        for current in np.arange(start, stop, step):
            #tasks.append(candles(symbol=symbol, interval=interval, limit=tick_limit, start=current, end=current+step))
            resp = await candles(symbol=symbol, interval=interval, limit=tick_limit, start=current, end=current+step)
            datas.extend(resp)
    else:
        resp = await candles(symbol=symbol, interval=interval, limit=tick_limit, start=start, end=stop)
        datas.extend(resp)
        

    # break up the task list into chunks
    #for task_chunks in chunks(tasks, 10):
    #    resps = await asyncio.gather(*task_chunks)
    #    for resp in resps:
    #        datas.extend(resp)


    return datas

async def main():
    # Define query parameters
    bin_size = '1m'
    limit = 1000
    time_step = 1000 * 60 * limit
    
    t_start = datetime.datetime(2013, 1, 1, 0, 0)
    t_start = time.mktime(t_start.timetuple()) * 1000
    
    t_stop = datetime.datetime.utcnow()
    t_stop = time.mktime(t_stop.timetuple()) * 1000
   
    async with aiohttp.ClientSession() as session:
        resp = await session.get('https://api.bitfinex.com/v1/symbols')
        pairs = await resp.json()
    
    SAVE_DIR = './data'
    
    if os.path.exists(SAVE_DIR) is False:
        os.mkdir(SAVE_DIR)

    for pair in pairs:
        dfs = []
        csv_path = '{}/{}.csv'.format(SAVE_DIR, pair.replace(':', ''))
    
        df = pd.DataFrame()
        if os.path.exists(csv_path):
            logger.info("data file exists: {}".format(csv_path))
            df = _read_file(csv_path)
            dfs.append(df)
        else:
            logger.info("data file does not exists, creating a new csv: {}".format(csv_path))
    
        if not df.empty:
            # get the latest time
            t_start = df.iloc[-1].time
            logger.info("Got existing csv, starting at : {}".format(t_start))

        if (t_stop - t_start) > 0:
            # only parse if there is new data to get
            pair_data = await fetch_data(start=t_start, stop=t_stop, symbol=pair, interval=bin_size, tick_limit=limit, step=time_step)

            # create a df from feteched data
            # Remove error messages
            ind = [np.ndim(x) != 0 for x in pair_data]
            pair_data = [i for (i, v) in zip(pair_data, ind) if v]
    
            # Create pandas data frame and clean data
            names = ['time', 'open', 'close', 'high', 'low', 'volume']
            df = pd.DataFrame(pair_data, columns=names)
            df.drop_duplicates(inplace=True)
            df.sort_values(by='time', ascending=True, inplace=True, ignore_index=True)
            dfs.append(df)

            # final df
            df = pd.concat(dfs, ignore_index=True)
            logger.info('Done downloading data. Saving to .csv.')
            df.to_csv(csv_path, index=False)
            logger.info('Done saving data. Moving to next pair.')

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    
    logger.info('Done retrieving data')
    loop.run_until_complete(main())
